/*

---------GAMEX----------
Button = PORTD7
LEDS = PORTC0-PORTC5
------------------------

*/
#ifndef __AVR_ATmega328P__
#define __AVR_ATmega328P__
#endif
#include <avr/io.h>
#include "util/delay.h"
#define F_CPU 16000000UL
#define DIFFICULTY 25 // 25-50-75 (HARD-MEDIUM-EASY)
#define BLINK_ITER 4

int stream[] = {0x00,0x01,0x03,0x07,0x0F,0x1F,0x3F};
void blink(int,int);

int main(void){
    DDRC = 0xFF;
    PORTC = 0x0;
    DDRD = 0x00;
    PORTD |= (1<<PD7);
    while (1)
    {
        for (int i = 0; i <= 6; i++)
        {
            PORTC = stream[i];
            _delay_ms(DIFFICULTY);
            if (!(PIND & (1<<7)))
            {
                blink(stream[i]);
                break;
            } 
        }
        for (int i = 6; i >= 0; i--)
        {
            PORTC = stream[i];
            _delay_ms(DIFFICULTY);
            if (!(PIND & (1<<7)))
            {
                blink(stream[i]);
                break;
            }
        }
    }
}
void blink(int value){
    for (int i = 0; i < BLINK_ITER; i++)
    {
        PORTC = value;
        _delay_ms(100);
        PORTC = 0x00;
        _delay_ms(100);
    }
}
